//
//  watherVC.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-02-02.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CoreLocation

class WeatherVC: UIViewController, CLLocationManagerDelegate{
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var cenditionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    
    let gradientLayer = CAGradientLayer()
    
    let apiKey = "9f52abbe48dc2b1ef080f5e8cdc60bbb"
    var lat = 11.344533
    var lon = 104.33322
    var activityIndicator: NVActivityIndicatorView!
    let locationManager = CLLocationManager()
    var temp = 0
    var temperature = 0
    var weather = ""
    var duomenys = [String:Any]()
    
    
    var shirtDictionary = [[Any]]()
    var shirtDictionary2 = [[Any]]()
    var pantsDictionary = [[Any]]()
    var pantsDictionary2 = [[Any]]()
    var sweatshirtsDictionary = [[Any]]()
    var sweatshirtsDictionary2 = [[Any]]()
    
    override func viewDidLoad() {
    super.viewDidLoad()
        backgroundView.layer.addSublayer(gradientLayer)
        
        let indicatorSize: CGFloat = 70
        let indicatorFrame = CGRect(x: (view.frame.width-indicatorSize)/2, y: (view.frame.height-indicatorSize)/2, width: indicatorSize, height: indicatorSize)
        activityIndicator = NVActivityIndicatorView(frame: indicatorFrame, type: .lineScale, color: UIColor.white, padding: 20.0)
        activityIndicator.backgroundColor = UIColor.black
        view.addSubview(activityIndicator)
        
        locationManager.requestWhenInUseAuthorization()
        
        activityIndicator.startAnimating()
        if(CLLocationManager.locationServicesEnabled()){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
 
        // Do any additional setup after loading the view.
    }
    
    func getData() -> [String:Any]{
        locationManager.requestWhenInUseAuthorization()
        if(CLLocationManager.locationServicesEnabled()){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        return duomenys
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setBlueGradientBackground()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        lat = location.coordinate.latitude
        lon = location.coordinate.longitude
        Alamofire.request("http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(apiKey)&units=metric").responseJSON { [self]
            response in
            
            
            
            self.activityIndicator.stopAnimating()
            if let responseStr = response.result.value {
                let jsonResponse = JSON(responseStr)
                let jsonWeather = jsonResponse["weather"].array![0]
                let jsonTemp = jsonResponse["main"]
                let iconName = jsonWeather["icon"].stringValue
                
                if self.locationLabel != nil {
                self.locationLabel.text = jsonResponse["name"].stringValue
                self.conditionImageView.image = UIImage(named: iconName)
                self.cenditionLabel.text = jsonWeather["main"].stringValue
                self.temperatureLabel.text = "\(Int(round(jsonTemp["temp"].doubleValue)))"
                
                self.temp = Int(round(jsonTemp["temp"].doubleValue))
                self.weather = jsonWeather["main"].stringValue
                self.temperature = (Int(round(jsonTemp["temp"].doubleValue)))
                self.duomenys = ["oras":self.weather,"temperature":self.temperature]
                
                let date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEEE"
                self.dayLabel.text = dateFormatter.string(from: date)
                
                let suffix = iconName.suffix(1)
                if(suffix == "n"){
                    self.setGreyGradientBackground()
                }else{
                    self.setBlueGradientBackground()
                }
            }
            }else {
                if let responseStr = response.result.value {
                    let jsonResponse = JSON(responseStr)
                    let jsonWeather = jsonResponse["weather"].array![0]
                    let jsonTemp = jsonResponse["main"]
                    let iconName = jsonWeather["icon"].stringValue
                    
                    
                    self.conditionImageView.image = UIImage(named: iconName)
                    self.weather = jsonWeather["main"].stringValue
                    self.temperature = (Int(round(jsonTemp["temp"].doubleValue)))
                    
                    self.temp = Int(round(jsonTemp["temp"].doubleValue))
                    
                    
                    duomenys = ["oras":self.weather,"temperature":self.temperature]
                    print("duomenys\(duomenys)")
                }
                
            }
        }
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    func setBlueGradientBackground(){
        let topColor = UIColor(red: 95.0/255.0, green: 165.0/255.0, blue: 1.0, alpha: 1.0).cgColor
        let bottomColor = UIColor(red: 72.0/255.0, green: 114.0/255.0, blue: 184.0/255.0, alpha: 1.0).cgColor
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor]
    }
    
    func setGreyGradientBackground(){
        let topColor = UIColor(red: 151.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 1.0).cgColor
        let bottomColor = UIColor(red: 72.0/255.0, green: 72.0/255.0, blue: 72.0/255.0, alpha: 1.0).cgColor
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor]
    }
    
    @IBAction func griztiTap(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "allClothes") as! AllClothesViewController

            self.present(newViewController, animated: true, completion: nil)
        newViewController.temp = temp
        newViewController.getInfo(temper: duomenys)
        
        shirtDictionary = [[UIImage(named: "shirt1")!,15,20,"Medvilne",75,"vasara ruduo"],
                      [UIImage(named: "shirt2")!,-10,15,"Sintetika",45,"vasara"],
                      [UIImage(named: "shirt3")!,10,19,"Medvilne",100,"ruduo"],
                      [UIImage(named: "shirt5")!,0,25,"sintetika",70,"vasara ruduo"],
                      [UIImage(named: "shirt6")!,13,14,"Medvilne",60,"vasara"],
                      [UIImage(named: "shirt7")!,-15,23,"Sintetika",35,"vasara ruduo"],
                      [UIImage(named: "shirt8")!,15,18,"Medvilne",20, "pavasaris"],
                      [UIImage(named: "shirt10")!,2,14,"Medvilne",80, "pavasaris"]]
        pantsDictionary = [[UIImage(named: "pants1")!,15,20,"Medvilne",75,"vasara ruduo"],
                      [UIImage(named: "pants2")!,-10,15,"Sintetika",45,"vasara"],
                      [UIImage(named: "pants3")!,10,19,"Medvilne",100,"ruduo"],
                      [UIImage(named: "pants5")!,0,25,"sintetika",70,"vasara ruduo"],
                      [UIImage(named: "pants9")!,13,14,"Medvilne",60,"vasara"],
                      [UIImage(named: "pants7")!,-15,23,"Sintetika",35,"vasara ruduo"],
                      [UIImage(named: "pants4")!,15,18,"Medvilne",20, "pavasaris"],
                      [UIImage(named: "pants10")!,2,14,"Medvilne",80, "pavasaris"]]
        sweatshirtsDictionary = [[UIImage(named: "megztinis1")!,15,20,"Medvilne",75,"vasara ruduo"],
                      [UIImage(named: "megztinis2")!,-10,15,"Sintetika",45,"vasara"],
                      [UIImage(named: "megztinis3")!,10,19,"Medvilne",100,"ruduo"],
                      [UIImage(named: "top5")!,0,25,"sintetika",70,"vasara ruduo"],
                      [UIImage(named: "top6")!,13,14,"Medvilne",60,"vasara"],
                      [UIImage(named: "top7")!,-15,23,"Sintetika",35,"vasara ruduo"],
                      [UIImage(named: "top8")!,15,18,"Medvilne",20, "pavasaris"],
                      [UIImage(named: "top10")!,2,14,"Medvilne",80, "pavasaris"]]
        
        for item in shirtDictionary{
            if (temp > item[1] as! Int && temp < item[2] as! Int) {
                shirtDictionary2.append(item)
            }
        }
        for item in pantsDictionary{
            if (temp > item[1] as! Int && temp < item[2] as! Int) {
                pantsDictionary2.append(item)
            }
        }
        for item in sweatshirtsDictionary{
            if (temp > item[1] as! Int && temp < item[2] as! Int) {
                sweatshirtsDictionary2.append(item)
            }
        }
        
//        newViewController.shirtDictionary2 = shirtDictionary2
//        newViewController.pantsDictionary2 = pantsDictionary2
//        newViewController.sweatshirtsDictionary2 = sweatshirtsDictionary2
        
    }
    
}
