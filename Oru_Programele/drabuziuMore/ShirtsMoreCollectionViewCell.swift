//
//  ShirtsMoreCollectionViewCell.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-01-06.
//

import UIKit

class ShirtsMoreCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var checkmarkLabel: UILabel!

    var isInEditingMode: Bool = false {
        didSet {
            checkmarkLabel.isHidden = !isInEditingMode
        }
    }
    

    override var isSelected: Bool {
        didSet {
           // if isInEditingMode {
                checkmarkLabel.text = isSelected ? "✓" : ""
            //}
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        checkmarkLabel.text = isSelected ? "✓" : ""
        super.awakeFromNib()
    }
}
