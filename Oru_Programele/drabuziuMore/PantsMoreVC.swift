//
//  pantsMore.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-01-06.
//

import UIKit

class PantsMoreViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    var pants = [UIImage]()
    var pantsDictionary = [[String:Any]]()
    @IBOutlet weak var pantsCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pantsCollectionView.dataSource = self
        pantsCollectionView.delegate = self
        pantsCollectionView.dragInteractionEnabled = true
        pantsCollectionView.dropDelegate = self
        pantsCollectionView.dragDelegate = self
        
        pants.append(UIImage(named: "pants1")!)
        pants.append(UIImage(named: "pants2")!)
        pants.append(UIImage(named: "pants3")!)
        pants.append(UIImage(named: "pants4")!)
        pants.append(UIImage(named: "pants5")!)
        pants.append(UIImage(named: "pants7")!)
        pants.append(UIImage(named: "pants8")!)
        pants.append(UIImage(named: "pants9")!)
        pants.append(UIImage(named: "pants10")!)
        
        print("data \(pantsDictionary)")
        
//        pantsDictionary = [[UIImage(named: "pants1")!,15,20,"Medvilne",75,"vasara ruduo"],
//                      [UIImage(named: "pants2")!,-10,15,"Sintetika",45,"vasara"],
//                      [UIImage(named: "pants3")!,10,19,"Medvilne",100,"ruduo"],
//                      [UIImage(named: "pants5")!,0,25,"sintetika",70,"vasara ruduo"],
//                      [UIImage(named: "pants9")!,13,14,"Medvilne",60,"vasara"],
//                      [UIImage(named: "pants7")!,-15,23,"Sintetika",35,"vasara ruduo"],
//                      [UIImage(named: "pants4")!,15,18,"Medvilne",20, "pavasaris"],
//                      [UIImage(named: "pants10")!,2,14,"Medvilne",80, "pavasaris"]]

    }
    func recieveData (data: [[String:Any]]){
        print("data2 \(pantsDictionary)")
        pantsDictionary = data
        
    }
    
    fileprivate func reorderItems(coordinator: UICollectionViewDropCoordinator, destination: IndexPath, collectionView: UICollectionView) {
        if let item = coordinator.items.first,
           let sourceIndexPath = item.sourceIndexPath {
            
            collectionView.performBatchUpdates({
                self.pantsDictionary.remove(at: sourceIndexPath.item)
                self.pantsDictionary.insert(contentsOf: item.dragItem.localObject as! [[String:Any]] , at: destination.item)
                
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destination])
                
            }, completion: nil )
            coordinator.drop(item.dragItem, intoItemAt: destination, rect: view.frame)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pantsDictionary.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "pictureDetailsVC") as! PictureDetailsVC
        var sezonai = ""

            self.present(newViewController, animated: true, completion: nil)
        newViewController.imageView.image = pantsDictionary[indexPath.item]["image"] as? UIImage
        newViewController.ivertinimasLabel.text = "\(pantsDictionary[indexPath.item]["like"] as! Int)"
        newViewController.medziagaLabel.text = pantsDictionary[indexPath.item]["fabric"] as? String
        newViewController.tempNuoLabel.text = "nuo: \(pantsDictionary[indexPath.item]["from"] as! Int) a"
        newViewController.tempIkiLabel.text = "iki: \(pantsDictionary[indexPath.item]["to"] as! Int)"
        for sezonas in pantsDictionary[indexPath.item]["sezon"] as! [String] {
            sezonai = sezonai + " " + sezonas
        }
        newViewController.sezonaiLabel.text = sezonai
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = pantsCollectionView.dequeueReusableCell(withReuseIdentifier: "PantsCell", for: indexPath) as! PantsMoreCollectionViewCell
        cell.imageView.image = pantsDictionary[indexPath.item]["image"] as! UIImage
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (view.frame.width/3)-20, height:(view.frame.width/3)-20);
        
    }
}

extension PantsMoreViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = pantsDictionary[indexPath.row]
        return [dragItem]
    }
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = pantsDictionary[indexPath.row]
        return [dragItem]
    }

}
extension PantsMoreViewController: UICollectionViewDropDelegate{
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation:  .forbidden)
    }

    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {

        let destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(item: row - 1 , section: 0)
        }

        if coordinator.proposal.operation == .move {
            self.reorderItems(coordinator: coordinator, destination: destinationIndexPath, collectionView: collectionView)
        }
    }
}
