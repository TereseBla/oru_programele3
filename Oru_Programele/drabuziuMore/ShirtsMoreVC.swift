//
//  ShirtsMore.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-01-06.
//

import UIKit

class ShirtsMoreViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var dictionary = [[String:Any]]()

    var shirts = [UIImage]()
    var editIsOn = Bool()
    var deleteIndexPaths = [IndexPath]()
    
    @IBOutlet weak var editbutton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    

    @IBOutlet weak var shirtsCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shirtsCollectionView.dataSource = self
        shirtsCollectionView.delegate = self
        shirtsCollectionView.dragInteractionEnabled = true
        shirtsCollectionView.dropDelegate = self
        shirtsCollectionView.dragDelegate = self
        shirtsCollectionView.allowsMultipleSelection = true
        
        print("data2\(dictionary)")
        
        deleteButton.tintColor = .gray
        deleteButton.isUserInteractionEnabled = false
        editIsOn = false
        
//        dictionary = [[UIImage(named: "shirt1")!,5,20,"Medvilne",75,"vasara ruduo"],
//                      [UIImage(named: "shirt2")!,4,15,"Sintetika",45,"vasara"],
//                      [UIImage(named: "shirt3")!,6,19,"Medvilne",100,"ruduo"],
//                      [UIImage(named: "shirt5")!,17,25,"sintetika",70,"vasara ruduo"],
//                      [UIImage(named: "shirt6")!,13,14,"Medvilne",60,"vasara"],
//                      [UIImage(named: "shirt7")!,14,23,"Sintetika",35,"vasara ruduo"],
//                      [UIImage(named: "shirt8")!,15,18,"Medvilne",20, "pavasaris"],
//                      [UIImage(named: "shirt10")!,2,14,"Medvilne",80, "pavasaris"]]
        
//        shirts.append(UIImage(named: "shirt1")!)
//        shirts.append(UIImage(named: "shirt2")!)
//        shirts.append(UIImage(named: "shirt3")!)
//        shirts.append(UIImage(named: "shirt5")!)
//        shirts.append(UIImage(named: "shirt6")!)
//        shirts.append(UIImage(named: "shirt7")!)
//        shirts.append(UIImage(named: "shirt8")!)
//        shirts.append(UIImage(named: "shirt10")!)
        
    }
    @IBAction func editTap(_ sender: Any) {
        
        if editIsOn == false {
            editbutton.setTitle("Baigti", for: .normal)
            deleteButton.tintColor = .systemBlue
            deleteButton.isUserInteractionEnabled = true
            editIsOn = true
        } else {
            editbutton.setTitle("Redaguoti", for: .normal)
            deleteButton.tintColor = .gray
            deleteButton.isUserInteractionEnabled = false
            editIsOn = false
        }
  
    }
    @IBAction func deleteTap(_ sender: Any) {
        
        for i in deleteIndexPaths.sorted(by: {$0.item < $1.item}) {
            dictionary.remove(at: i.item)
        }
        
        shirtsCollectionView.deleteItems(at: deleteIndexPaths)
    }
    
    func recieveData (data: [[String:Any]]){
        
        
        print("data\(data)")
        dictionary = data
        
    }
    func addedImg(dict:[[String:Any]]) {
        dictionary = dict
        //dictionary.append(dict)
        print("dict2\(dictionary)")
    }
    @IBAction func addShirt(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "pickShirt") as! PickShirtVC

        
        //shirts.append(img)
        self.present(newViewController, animated: true, completion: nil)
        newViewController.getData(data:dictionary)
    }
    
    fileprivate func reorderItems(coordinator: UICollectionViewDropCoordinator, destination: IndexPath, collectionView: UICollectionView) {
        if let item = coordinator.items.first,
           let sourceIndexPath = item.sourceIndexPath {
            
            collectionView.performBatchUpdates({
                self.dictionary.remove(at: sourceIndexPath.item)
               // self.dictionary.insert (item.dragItem.localObject as! [[String:Any]] , at: destination.item)
                self.dictionary.insert (item.dragItem.localObject as! [String : Any]  , at: destination.item)
                
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destination])
                
            }, completion: nil )
            coordinator.drop(item.dragItem, intoItemAt: destination, rect: view.frame)

        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dictionary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = shirtsCollectionView.dequeueReusableCell(withReuseIdentifier: "ShirtsCell", for: indexPath) as! ShirtsMoreCollectionViewCell
        cell.imageView.image = dictionary[indexPath.item]["image"] as! UIImage
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if editIsOn == false {
            var cell = collectionView.cellForItem(at: indexPath) as! ShirtsMoreCollectionViewCell
            cell.checkmarkLabel.isHidden = true
            //cell.isSelected = false
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "pictureDetailsVC") as! PictureDetailsVC
            var sezonai = ""

            self.present(newViewController, animated: true, completion: nil)
            newViewController.imageView.image = dictionary[indexPath.item]["image"] as? UIImage
            newViewController.ivertinimasLabel.text = "\(dictionary[indexPath.item]["like"] as! Int)"
           // newViewController.medziagaLabel.text = dictionary[indexPath.item]["fabric"] as? String
            newViewController.tempNuoLabel.text = "nuo: \(dictionary[indexPath.item]["from"] as! Int)"
            newViewController.tempIkiLabel.text = "iki: \(dictionary[indexPath.item]["to"] as! Int)"
            for sezonas in dictionary[indexPath.item]["sezon"] as! [String] {
                sezonai = sezonai + " " + sezonas
            }
            newViewController.sezonaiLabel.text = sezonai
        }else {
            
            var cell = collectionView.cellForItem(at: indexPath) as! ShirtsMoreCollectionViewCell
            cell.isSelected = true
            deleteIndexPaths.append(indexPath)
            cell.checkmarkLabel.isHidden = false
            
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (view.frame.width / 3) - 20, height:(view.frame.width / 3) - 20);
        
    }


}
extension ShirtsMoreViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = dictionary[indexPath.row]
        return [dragItem]
    }
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = dictionary[indexPath.row]
        return [dragItem]
    }

}
extension ShirtsMoreViewController: UICollectionViewDropDelegate{
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation:  .forbidden)
    }

    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {

        let destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(item: row - 1 , section: 0)
        }

        if coordinator.proposal.operation == .move {
            self.reorderItems(coordinator: coordinator, destination: destinationIndexPath, collectionView: collectionView)
        }
    }
}
