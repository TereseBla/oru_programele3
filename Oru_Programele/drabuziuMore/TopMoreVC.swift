//
//  TopMoreViewController.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-01-06.
//

import UIKit

class TopMoreViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var tops = [UIImage]()
    var sweatshirtsDictionary = [[String:Any]]()

    @IBOutlet weak var topscollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topscollectionView.dataSource = self
        topscollectionView.delegate = self
        topscollectionView.dragInteractionEnabled = true
        topscollectionView.dropDelegate = self
        topscollectionView.dragDelegate = self
        
        tops.append(UIImage(named: "megztinis1")!)
        tops.append(UIImage(named: "megztinis2")!)
        tops.append(UIImage(named: "megztinis3")!)
        tops.append(UIImage(named: "top5")!)
        tops.append(UIImage(named: "top6")!)
        tops.append(UIImage(named: "top7")!)
        tops.append(UIImage(named: "top8")!)
        tops.append(UIImage(named: "top9")!)
        tops.append(UIImage(named: "top10")!)
//        sweatshirtsDictionary = [[UIImage(named: "megztinis1")!,15,20,"Medvilne",75,"vasara ruduo"],
//                      [UIImage(named: "megztinis2")!,-10,15,"Sintetika",45,"vasara"],
//                      [UIImage(named: "megztinis3")!,10,19,"Medvilne",100,"ruduo"],
//                      [UIImage(named: "top5")!,0,25,"sintetika",70,"vasara ruduo"],
//                      [UIImage(named: "top6")!,13,14,"Medvilne",60,"vasara"],
//                      [UIImage(named: "top7")!,-15,23,"Sintetika",35,"vasara ruduo"],
//                      [UIImage(named: "top8")!,15,18,"Medvilne",20, "pavasaris"],
//                      [UIImage(named: "top10")!,2,14,"Medvilne",80, "pavasaris"]]

    }
    func recieveData (data: [[String:Any]]){
        sweatshirtsDictionary = data
        
    }
    
    fileprivate func reorderItems(coordinator: UICollectionViewDropCoordinator, destination: IndexPath, collectionView: UICollectionView) {
        if let item = coordinator.items.first,
           let sourceIndexPath = item.sourceIndexPath {
            
            collectionView.performBatchUpdates({
                self.sweatshirtsDictionary.remove(at: sourceIndexPath.item)
                self.sweatshirtsDictionary.insert(contentsOf: item.dragItem.localObject as! [[String:Any]], at: destination.item)
                
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destination])
                
            }, completion: nil )
            coordinator.drop(item.dragItem, intoItemAt: destination, rect: view.frame)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sweatshirtsDictionary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = topscollectionView.dequeueReusableCell(withReuseIdentifier: "TopCell", for: indexPath) as! TopMoreCollectionViewCell
        cell.imageView.image = sweatshirtsDictionary[indexPath.item]["image"] as! UIImage
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (view.frame.width/3)-20, height:(view.frame.width/3)-20);
        
    }
}

extension TopMoreViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = sweatshirtsDictionary[indexPath.row]
        return [dragItem]
    }
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = sweatshirtsDictionary[indexPath.row]
        return [dragItem]
    }

}
extension TopMoreViewController: UICollectionViewDropDelegate{
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation:  .forbidden)
    }

    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {

        let destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(item: row - 1 , section: 0)
        }

        if coordinator.proposal.operation == .move {
            self.reorderItems(coordinator: coordinator, destination: destinationIndexPath, collectionView: collectionView)
        }
    }
}

