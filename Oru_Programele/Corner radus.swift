//
//  Corner radus.swift
//  secolinkApp.2
//
//  Created by Terese Blazauskaite on 22/10/2019.
//  Copyright © 2019 Terese Blazauskaite. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableView: UIView {
}
extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}
