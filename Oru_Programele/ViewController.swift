//
//  ViewController.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-01-05.
//

import UIKit
import Firebase
import FirebaseDatabase
import GoogleSignIn


class ViewController: UIViewController, GIDSignInDelegate{
    
    @IBOutlet weak var elPastoTextField: UITextField!
    @IBOutlet weak var slaptazodzioTextField: UITextField!
    @IBOutlet weak var pranesimoLaukas: UILabel!
    
    @IBOutlet weak var googleSignInButton: GIDSignInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        slaptazodzioTextField.isSecureTextEntry = true
        slaptazodzioTextField.leftViewMode = .always
        slaptazodzioTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
        elPastoTextField.autocapitalizationType = .none
        elPastoTextField.leftViewMode = .always
        elPastoTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
        
       
    }
    
    @IBAction func appleidLogInTap(_ sender: Any) {
        
        
        
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil {
            
            guard let authentication = user.authentication else { return }
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                                accessToken: authentication.accessToken)
            
            Auth.auth().signInAndRetrieveData(with: credential) { _,_ in
                if let error = error {
                    print ("Firebase sign in error")
                }
                
            }
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "allClothes") as! AllClothesViewController

            self.present(newViewController, animated: true, completion: nil)
        }
    }
    func addNewEntry(){
        let database = Database.database().reference()
        let object: [String:Any] = [
            "name": "ios" as NSObject,
            "something": "vc"
        ]
        database.child("somethingID").setValue(object)
    }


    @IBAction func prisijungtiPressed(_ sender: Any) {
        
        guard let email = elPastoTextField.text, !email.isEmpty, let password = slaptazodzioTextField.text, !password.isEmpty else {
            print("neivesti duomenys")
            return
        }
        
        FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password, completion: { [weak self] result, error in
            guard let strongSelf = self else {
                return
            }
            guard error == nil else {
                
                strongSelf.showCreateAccount(email: email, password: password)
                return
            }
            print("Signed in")
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "allClothes") as! AllClothesViewController

            self?.present(newViewController, animated: true, completion: nil)
            
        })
        
        
//        if elPastoTextField.text != "test1@gmail.com" {
//            pranesimoLaukas.text = "Neteisingai ivesti duomenys"
//            pranesimoLaukas.isHidden = false
//            
      //  } else {
//            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let newViewController = storyBoard.instantiateViewController(withIdentifier: "allClothes") as! AllClothesViewController
//
//                self.present(newViewController, animated: true, completion: nil)
        //}
    }
    
    func showCreateAccount(email: String, password: String){
        let alert = UIAlertController(title: "Sukurti anketa", message: "Ar norite sukurti anketa?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Testi", style: .default, handler: {_ in
            FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password, completion: {[weak self]result, error in
                guard let strongSelf = self else {
                    return
                }
                guard error == nil else {
                    
                    print("Account creation failed")
                    return
                }
                print("Account created")
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "allClothes") as! AllClothesViewController

                self?.present(newViewController, animated: true, completion: nil)
            })
        }))
        alert.addAction(UIAlertAction(title: "Atsaukti", style: .cancel, handler: {_ in
            
        }))
        present(alert, animated: true)
        
    }
    
    
    
    
    
}

