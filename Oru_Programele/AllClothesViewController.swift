//
//  AllClothesViewController.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-01-06.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CoreLocation





class AllClothesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collectionView1: UICollectionView!
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var collectionView3: UICollectionView!
    var shirts = [UIImage]()
    var pants = [UIImage]()
    var tops = [UIImage]()
    var shirtDictionary = [[String:Any]]()
    var shirtDictionary2 = [[String:Any]]()
    var shirtDictionary3 = [[String:Any]]()
    var pantsDictionary = [[String:Any]]()
    var pantsDictionary2 = [[String:Any]]()
    var pantsDictionary3 = [[String:Any]]()
    var sweatshirtsDictionary = [[String:Any]]()
    var sweatshirtsDictionary2 = [[String:Any]]()
    var sweatshirtsDictionary3 = [[String:Any]]()
    var temperatureData = [String:Any]()
    var temp = 13
    var calculateClass = CalculateBest()
    var weatherVC = WeatherVC()
    var shirtsMoreVC = ShirtsMoreViewController()
    var pantsMoreVC = PantsMoreViewController()
    var topMoreVC = TopMoreViewController()
    var temperature = 0
    var weather = ""
    var shirtDictionaryExample = [clothe]()
    
    @IBOutlet weak var zinutesTekstas: UILabel!
    
    struct clothe {
        var image: UIImage
        var from: Int // This should probably be a URL, NSImage or UIImage
        let to: Int
        let fabric: String
        let like: Int
        let sezon: [String]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView1.delegate = self
        collectionView1.dataSource = self
        collectionView2.delegate = self
        collectionView2.dataSource = self
        collectionView3.delegate = self
        collectionView3.dataSource = self
        shirtDictionaryExample = [clothe(image: UIImage(named: "shirt1")!, from: 18, to: 30, fabric: "Medvilne", like: 75, sezon: ["vasara"]),
        clothe(image: UIImage(named: "shirt2")!, from: 18, to: 30, fabric: "Sintetika", like: 45, sezon: ["vasara"]),
        clothe(image: UIImage(named: "shirt3")!, from: 18, to: 30, fabric: "Medvilne", like: 100, sezon: ["ruduo"])]
        
        var example = [["image": UIImage(named: "shirt1")!, "from": 18, "to": 30, "fabric": "Medvilne", "like": 75, "sezon": ["vasara"]],
                       ["image": UIImage(named: "shirt2")!, "from": 18, "to": 30, "fabric": "Sintetika", "like": 45, "sezon": ["vasara"]],
                       ["image": UIImage(named: "shirt3")!, "from": 18, "to": 30, "fabric": "Medvilne", "like": 100, "sezon": ["ruduo"]]] as [[String : Any]]
        
        shirtDictionary = [["image":UIImage(named: "shirt1")!,"from":18,"to":30,"fabric":"Medvilne","like":75,"sezon":["vasara"]],
                      ["image":UIImage(named: "shirt2")!,"from":18,"to":30,"fabric":"Sintetika","like":45,"sezon":["vasara"]],
                      ["image":UIImage(named: "shirt3")!,"from":18,"to":30,"fabric":"Medvilne","like":100,"sezon":["ruduo"]],
                      ["image":UIImage(named: "shirt5")!,"from":18,"to":30,"fabric":"sintetika","like":70,"sezon":["vasara"]],
                      ["image":UIImage(named: "shirt6")!,"from":15,"to":20,"fabric":"Medvilne","like":60,"sezon":["vasara", "ruduo", "pavasaris"]],
                      ["image":UIImage(named: "shirt7")!,"from":15,"to":20,"fabric":"Sintetika","like":35, "sezon":["vasara", "ruduo", "pavasaris"]],
                      ["image":UIImage(named: "shirt8")!,"from":18,"to":30,"fabric":"Medvilne","like":20, "sezon":["vasara"]],
                      ["image":UIImage(named: "shirt10")!,"from":18,"to":30,"fabric":"Medvilne","like":80, "sezon":["vasara"]],
                      ["image":UIImage(named: "megztinis1")!,"from":15,"to":20,"fabric":"Medvilne","like":75,"sezon":["ziema", "ruduo", "pavasaris"]],
                      ["image":UIImage(named: "megztinis2")!,"from":-10,"to":15,"fabric":"Sintetika","like":45,"sezon":["ziema", "ruduo", "pavasaris"]],
                      ["image":UIImage(named: "megztinis3")!,"from":10,"to":19,"fabric":"Medvilne","like":100,"sezon":["ziema", "ruduo", "pavasaris"]],
                      ["image":UIImage(named: "top5")!,"from":-10,"to":10,"fabric":"sintetika","like":70,"sezon":["ziema", "ruduo"]],
                      ["image":UIImage(named: "top6")!,"from":0,"to":15,"fabric":"Medvilne","like":60,"sezon":["pavasaris", "ruduo"]],
                      ["image":UIImage(named: "top7")!,"from":0,"to":15,"fabric":"Sintetika","like":35,"sezon":["pavasaris", "ruduo"]],
                      ["image":UIImage(named: "top8")!,"from":5,"to":15,"fabric":"Medvilne","like":20, "sezon":["ziema", "vasara","ruduo"]],
                      ["image":UIImage(named: "top9")!,"from":-10,"to":10,"fabric":"sintetika","like":70,"sezon":["ziema", "ruduo"]],
                      ["image":UIImage(named: "top10")!,"from":-10,"to":10,"fabric":"Medvilne","like":80, "sezon":["ziema", "ruduo"]]]
        pantsDictionary = [["image":UIImage(named: "skirt1")!,"from":15,"to":25,"fabric":"Medvilne","like":75,"sezon":["pavasaris", "ruduo", "vasara"]],
                           ["image":UIImage(named: "skirt3")!,"from":15,"to":25,"fabric":"Sintetika","like":45,"sezon":["vasara"]],
                           ["image":UIImage(named: "pants1")!,"from":10,"to":20,"fabric":"Medvilne","like":75,"sezon":["pavasaris", "ruduo"]],
                      ["image":UIImage(named: "pants2")!,"from":-10,"to":15,"fabric":"Sintetika","like":45,"sezon":["ziema", "ruduo", "pavasaris", "vasara"]],
                      ["image":UIImage(named: "pants3")!,"from":10,"to":19,"fabric":"Medvilne","like":100,"sezon":["ziema", "ruduo", "pavasaris", "vasara"]],
                      ["image":UIImage(named: "pants5")!,"from":-10,"to":20,"fabric":"sintetika","like":0,"sezon":["ziema", "ruduo", "pavasaris", "vasara"]],
                      ["image":UIImage(named: "pants9")!,"from":0,"to":18,"fabric":"Medvilne","like":60,"sezon":["ziema", "ruduo", "pavasaris", "vasara"]],
                      ["image":UIImage(named: "pants7")!,"from":0,"to":20,"fabric":"Sintetika","like":35,"sezon":["ruduo", "pavasaris", "vasara"]],
                      ["image":UIImage(named: "pants4")!,"from":0,"to":18,"fabric":"Medvilne","like":20,"sezon":[ "ruduo", "pavasaris", "vasara"]],
                      ["image":UIImage(named: "pants10")!,"from":2,"to":15,"fabric":"Medvilne","like":10,"sezon":["ruduo", "pavasaris", "vasara"]],
                      ]
        sweatshirtsDictionary = [
            ["image":UIImage(named: "dress1")!,"from":10,"to":25,"fabric":"Medvilne","like":100,"sezon":["pavasaris", "vasara"]],
            ["image":UIImage(named: "dress2")!,"from":15,"to":25,"fabric":"sintetika","like":70,"sezon":["pavasaris", "vasara"]],
            ["image":UIImage(named: "dress3")!,"from":18,"to":25,"fabric":"Medvilne","like":60,"sezon":["pavasaris", "vasara"]],
            ["image":UIImage(named: "dress4")!,"from":-15,"to":10,"fabric":"Sintetika","like":35,"sezon":["ziema", "ruduo"]],
            ["image":UIImage(named: "dress5")!,"from":10,"to":20,"fabric":"Medvilne","like":20, "sezon":["pavasaris", "vasara", "ruduo"]],
            ["image":UIImage(named: "jumpsuit1")!,"from":15,"to":24,"fabric":"Medvilne","like":75, "sezon":["pavasaris", "vasara"]],
            ["image":UIImage(named: "jumpsuit2")!,"from":0,"to":15,"fabric":"Medvilne","like":25,"sezon": ["pavasaris", "ruduo"]],
            ["image":UIImage(named: "jumpsuit3")!,"from":2,"to":14,"fabric":"Medvilne","like":10,"sezon": ["pavasaris", "vasara"]],
            ["image":UIImage(named: "jumpsuit4")!,"from":-5,"to":15,"fabric":"Medvilne","like":85,"sezon": ["ruduo", "ziema"]],
            ["image":UIImage(named: "jumpsuit5")!,"from":2,"to":14,"fabric":"Medvilne","like":65, "sezon":["pavasaris", "ruduo"]],
            ["image":UIImage(named: "jumpsuit6")!,"from":18,"to":30,"fabric":"Medvilne","like":80,"sezon": ["vasara"]]]
        
        
           // shirtDictionaryExample.sorted{ $0.like > $1.like }
       // sort(&shirtDictionaryExample) {$0.like > $1.like}
        print("example\(shirtDictionaryExample)")
        
//        var returnData = [[String:Any]]()
//            returnData = example
//            returnData.sort{
//                    let created_date0 = $0["like"] as? Int ?? 0
//                    let created_date1 = $1["like"] as? Int ?? 0
//                    return created_date0 < created_date1
//            }
//        print("example2\(returnData)")
        
       // shirtDictionary.sorted {$0[4], $1[4]}
       // shirtDictionary.sort { ($0[4] as AnyObject).compare($1[4]) == .orderedAscending }
       // print("sorted \(shirtDictionary)")
//        shirtDictionary.sort { (lhs: [Any], rhs: [Any]) -> Bool in
//            return lhs[4] > rhs[4]
//        }
        
//        for item in shirtDictionary{
//            if (temp > item[1] as! Int && temp < item[2] as! Int) {
//                shirtDictionary2.append(item)
//            }
//        }
//        for item in pantsDictionary{
//            if (temp > item[1] as! Int && temp < item[2] as! Int) {
//                pantsDictionary2.append(item)
//            }
//        }
//        for item in sweatshirtsDictionary{
//            if (temp > item[1] as! Int && temp < item[2] as! Int) {
//                sweatshirtsDictionary2.append(item)
//            }
//        }
       // var oruDuom = weatherVC.getData()
       // print("orai\(oruDuom)")
       // calculateClass.getClothesData(shirtsData: shirtDictionary, pantsData: pantsDictionary, sweatshirtsData: sweatshirtsDictionary, oruDuom: oruDuom)
        //shirtsMoreVC.recieveData(data: shirtDictionary)
       // pantsMoreVC.recieveData(data: pantsDictionary)
        //topMoreVC.recieveData(data: sweatshirtsDictionary)

    }

    
    func getInfo(temper:[String:Any]){
        print("temperatura \(temper)")
        temperatureData = temper
        temperature = (temperatureData["temperature"] as? Int)!
        weather = (temperatureData["oras"] as? String)!
        
        changeDictionaries()
        
        
    }
    func changeDictionaries(){
        shirtDictionary2.removeAll()
        shirtDictionary3.removeAll()
        pantsDictionary2.removeAll()
        pantsDictionary3.removeAll()
        sweatshirtsDictionary2.removeAll()
        sweatshirtsDictionary3.removeAll()
        for item in shirtDictionary{
            if (temperature > item["from"] as! Int && temperature < item["to"] as! Int) {
                
                shirtDictionary2.append(item)
                
            } else {
                shirtDictionary3.append(item)
            }
            
        }
        for item in pantsDictionary{
            if (temperature > item["from"] as! Int && temperature < item["to"] as! Int) {
                pantsDictionary2.append(item)
            } else {
                pantsDictionary3.append(item)
            }
            print("pantsd\(pantsDictionary2)")
            print("pantsd2\(pantsDictionary3)")
        }
        for item in sweatshirtsDictionary{
            if (temperature > item["from"] as! Int && temperature < item["to"] as! Int) {
                sweatshirtsDictionary2.append(item)
            } else {
                sweatshirtsDictionary3.append(item)
            }
        }
        var returnDataShirts1 = [[String:Any]]()
            returnDataShirts1 = shirtDictionary2
            returnDataShirts1.sort{
                    let created_date0 = $0["like"] as? Int ?? 0
                    let created_date1 = $1["like"] as? Int ?? 0
                    return created_date0 > created_date1
            }
        var returnDataShirts2 = [[String:Any]]()
        returnDataShirts2 = shirtDictionary3
        returnDataShirts2.sort{
                    let created_date0 = $0["like"] as? Int ?? 0
                    let created_date1 = $1["like"] as? Int ?? 0
                    return created_date0 > created_date1
            }
        var returnDataPants1 = [[String:Any]]()
        returnDataPants1 = pantsDictionary2
        returnDataPants1.sort{
                    let created_date0 = $0["like"] as? Int ?? 0
                    let created_date1 = $1["like"] as? Int ?? 0
                    return created_date0 > created_date1
            }
        var returnDataPants2 = [[String:Any]]()
        returnDataPants2 = pantsDictionary3
        returnDataPants2.sort{
                    let created_date0 = $0["like"] as? Int ?? 0
                    let created_date1 = $1["like"] as? Int ?? 0
                    return created_date0 > created_date1
            }
        var returnDatafull1 = [[String:Any]]()
        returnDatafull1 = sweatshirtsDictionary2
        returnDatafull1.sort{
                    let created_date0 = $0["like"] as? Int ?? 0
                    let created_date1 = $1["like"] as? Int ?? 0
                    return created_date0 > created_date1
            }
        var returnDatafull2 = [[String:Any]]()
        returnDatafull2 = sweatshirtsDictionary3
        returnDatafull2.sort{
                    let created_date0 = $0["like"] as? Int ?? 0
                    let created_date1 = $1["like"] as? Int ?? 0
                    return created_date0 > created_date1
            }
        
        
        
        print("data\(shirtDictionary2)")
        print("example\(shirtDictionaryExample)")
        shirtDictionary.removeAll()
        pantsDictionary.removeAll()
        sweatshirtsDictionary.removeAll()
        shirtDictionary.append(contentsOf: returnDataShirts1)
        shirtDictionary.append(contentsOf: returnDataShirts2)
        pantsDictionary.append(contentsOf: returnDataPants1)
        pantsDictionary.append(contentsOf: returnDataPants2)
        sweatshirtsDictionary.append(contentsOf: returnDatafull1)
        sweatshirtsDictionary.append(contentsOf: returnDatafull2)
        collectionView1.reloadData()
        collectionView2.reloadData()
        collectionView3.reloadData()
        
        
        
    }
    @IBAction func fullBodyMoreTap(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "tops") as! TopMoreViewController

            self.present(newViewController, animated: true, completion: nil)
        newViewController.recieveData(data: sweatshirtsDictionary)
    }
    @IBAction func pantsMoreTap(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "pants") as! PantsMoreViewController

            self.present(newViewController, animated: true, completion: nil)
        newViewController.recieveData(data: pantsDictionary)
    }
    @IBAction func shirtsMoreButtonTap(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "shirts") as! ShirtsMoreViewController

            self.present(newViewController, animated: true, completion: nil)
        newViewController.recieveData(data: shirtDictionary)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView1{
            
            return shirtDictionary.count
        } else if collectionView == collectionView2{
            
            return pantsDictionary.count
        } else if collectionView == collectionView3 {
            
            return sweatshirtsDictionary.count
        }
        else {
            preconditionFailure("Unknown collection view!")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionView1{
            let cell = collectionView1.dequeueReusableCell(withReuseIdentifier: "Cell1", for: indexPath) as! ClotheCollectionViewCell
           // cell.imageView.image = shirts[indexPath.item]
            cell.imageView.image = shirtDictionary[indexPath.item]["image"] as? UIImage
            return cell
        } else if collectionView == collectionView2{
            let cell = collectionView2.dequeueReusableCell(withReuseIdentifier: "Cell2", for: indexPath) as! ClotheCollectionViewCell2
            //cell.imageView.image = pants[indexPath.item]
            cell.imageView.image = pantsDictionary[indexPath.item]["image"] as? UIImage
            return cell
        } else if collectionView == collectionView3 {
            let cell = collectionView3.dequeueReusableCell(withReuseIdentifier: "Cell3", for: indexPath) as! ClotheCollectionViewCell3
            //cell.imageView.image = tops[indexPath.item]
            cell.imageView.image = sweatshirtsDictionary[indexPath.item]["image"] as? UIImage
            return cell
        }
        else {
            preconditionFailure("Unknown collection view!")
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            return CGSize(width: 50, height: 50);
        
    }
}
