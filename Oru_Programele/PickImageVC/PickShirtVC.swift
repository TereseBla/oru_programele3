//
//  PickShirtVC.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-02-02.
//

import Foundation
import UIKit

class PickShirtVC: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource{
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pickerMedziaga: UIPickerView!
    
    var pickerMedziagaData: [String] = [String]()
    var pantsMorevc = PantsMoreViewController()
    var imagePicker = UIImagePickerController()
    var img = UIImage()
    var selectedMedziaga = ""
    var newDict = [String : Any]()
    var dict = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        //self.pickerMedziaga.delegate = self
        //self.pickerMedziaga.dataSource = self
        //pickerMedziagaData = ["Sintetika", "Linas", "Medvilne", "Silkas"]
        
        // Do any additional setup after loading the view.
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
       {
        //selectedMedziaga = pickerMedziagaData[row]

        }
    func getData(data:[[String:Any]]){
        dict = data
    }
    
    @IBAction func onClickPick(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func onClickAdd(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "shirts") as! ShirtsMoreViewController

        
        //shirts.append(img)
        self.present(newViewController, animated: true, completion: nil)
        newDict = ["image":img,"from": 10,"to": 20,"fabric":selectedMedziaga, "like":100, "sezon" :["vasara"]]
        
        dict.append(newDict)
        print("dict\(dict)")
        newViewController.addedImg(dict:dict)
       // newViewController.dictionary.append(newDict)
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerMedziagaData.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(pickerMedziagaData[row])"
    }
    
}
extension PickShirtVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            imageView.image = image
            img = image
        }
        dismiss(animated: true, completion: nil)
    }
}
