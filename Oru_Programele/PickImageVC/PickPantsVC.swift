//
//  pickPantsVC.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-02-02.
//

import Foundation
import UIKit

class PickPantsVC: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource{
    
    @IBOutlet weak var pickerMedziaga: UIPickerView!
    @IBOutlet weak var imageView: UIImageView!
    var pickerMedziagaData: [String] = [String]()
    var pantsMorevc = PantsMoreViewController()
    var imagePicker = UIImagePickerController()
    var img = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.pickerMedziaga.delegate = self
        self.pickerMedziaga.dataSource = self
        pickerMedziagaData = ["Dzinsai", "Linas", "Medvilne", "Silkas"]
        
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickPick(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func onClickAdd(_ sender: Any) {
        
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "pants") as! PantsMoreViewController
//
//        newViewController.pants.append(img)
//            self.present(newViewController, animated: true, completion: nil)
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerMedziagaData.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(pickerMedziagaData[row])"
    }
}
extension PickPantsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            imageView.image = image
            img = image
        }
        dismiss(animated: true, completion: nil)
    }
    
}
