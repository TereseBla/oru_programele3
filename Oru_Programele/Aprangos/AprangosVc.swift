//
//  AprangosVc.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-02-03.
//

import Foundation
import UIKit

class AprangosVc: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var aprangosColectionView: UICollectionView!
    var aprangos = [UIImage]()
       override func viewDidLoad() {
        super.viewDidLoad()
        aprangosColectionView.delegate = self
        aprangosColectionView.dataSource = self
        aprangosColectionView.dragInteractionEnabled = true
        
        aprangos.append(UIImage(named: "outfit1")!)
        aprangos.append(UIImage(named: "outfit2")!)
        aprangos.append(UIImage(named: "outfit3")!)
        aprangos.append(UIImage(named: "outfit4")!)
        aprangos.append(UIImage(named: "outfit5")!)
        aprangos.append(UIImage(named: "outfit6")!)
        aprangos.append(UIImage(named: "outfi7")!)
        aprangos.append(UIImage(named: "outfit8")!)
        aprangos.append(UIImage(named: "outfit9")!)
        aprangos.append(UIImage(named: "outfit10")!)
        aprangos.append(UIImage(named: "outfit11")!)
        aprangos.append(UIImage(named: "outfit12")!)
        aprangos.append(UIImage(named: "outfit13")!)
        // Do any additional setup after loading the view.
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(_:)))
        aprangosColectionView?.addGestureRecognizer(gesture)
    }
    fileprivate func reorderItems(coordinator: UICollectionViewDropCoordinator, destination: IndexPath, collectionView: UICollectionView) {
        if let item = coordinator.items.first,
           let sourceIndexPath = item.sourceIndexPath {
            
            collectionView.performBatchUpdates({
                self.aprangos.remove(at: sourceIndexPath.item)
                self.aprangos.insert(item.dragItem.localObject as! UIImage, at: destination.item)
                
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destination])
                
            }, completion: nil )
            coordinator.drop(item.dragItem, intoItemAt: destination, rect: view.frame)

        }
    }
    @objc func handleLongPressGesture(_ gesture: UILongPressGestureRecognizer){
        guard let aprangosCollection = aprangosColectionView else {
            return
        }
        switch gesture.state {
        case .began:
            guard let targetIndexPath = aprangosColectionView.indexPathForItem(at: gesture.location(in: aprangosColectionView)) else {
                return
            }
            aprangosColectionView.beginInteractiveMovementForItem(at: targetIndexPath)
        case .changed:
            aprangosColectionView.updateInteractiveMovementTargetPosition(gesture.location(in: aprangosColectionView))
        case .ended:
            aprangosColectionView.endInteractiveMovement()
        default:
            aprangosColectionView.cancelInteractiveMovement()
        }
    }
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = aprangos.remove(at: sourceIndexPath.row)
        aprangos.insert(item, at: destinationIndexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aprangos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = aprangosColectionView.dequeueReusableCell(withReuseIdentifier: "AprangosCell", for: indexPath) as! AprangosCollectionViewCell
            cell.imageView.image = aprangos[indexPath.item]
            return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            return CGSize(width: 200, height: 200);

    }

}
extension AprangosVc: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = aprangos[indexPath.row]
        return [dragItem]
    }
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = aprangos[indexPath.row]
        return [dragItem]
    }

}
extension AprangosVc: UICollectionViewDropDelegate{
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation:  .forbidden)
    }

    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {

        let destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(item: row - 1 , section: 0)
        }

        if coordinator.proposal.operation == .move {
            self.reorderItems(coordinator: coordinator, destination: destinationIndexPath, collectionView: collectionView)
        }
    }
}
