//
//  Registration.swift
//  Oru_Programele
//
//  Created by Terese Blazauskaite on 2021-01-06.
//

import Foundation
import UIKit
import FirebaseDatabase

class Registration: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var mailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var errorText: UILabel!
    var pickerData: [String] = [String]()
    @IBOutlet weak var picker: UIPickerView!
    
    var selectedType = ""
    var pickerIsSelected = false
    
    private let database = Database.database().reference()
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
        errorText.isHidden = true
        self.picker.delegate = self
        self.picker.dataSource = self
        
        
        passwordText.isSecureTextEntry = true
        repeatPassword.isSecureTextEntry = true
        pickerData = ["I(0)", "II(A)", "III(B)", "IV(AB)"]

    }
    
    func addNewEntry(){
        let vardas = nameText.text
        let pastas = mailText.text
        let slapt = passwordText.text
        //let slapt2 = repeatPassword.text
        let age = ageText.text
        let bloodType = selectedType
        
        
        if ((nameText.text!.count > 0) || (mailText.text!.count > 0) || (passwordText.text!.count > 0) || (repeatPassword.text!.count > 0) || (ageText.text!.count > 0) || pickerIsSelected == true){
            errorText.isHidden = true
        let object: [String:Any] = [
            "vardas": vardas!,
            "el pastas": pastas!,
            "slaptazodis": slapt!,
            "amzius": age!,
            "kraujo grupe" : bloodType
        ]
        database.child("ID_\(Int.random(in: 0..<100))").setValue(object)
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController

                self.present(newViewController, animated: true, completion: nil)
        }
//        } else if slapt != slapt2 {
//            errorText.text = "Slaptazodziai nesutampa"
//            errorText.isHidden = false
//        }
        else {
            errorText.text = "Ne visi laukai uzpildyti"
            errorText.isHidden = false
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
       {

        selectedType = pickerData[row]
        pickerIsSelected = true
        }
    @IBAction func registruotisTapp(_ sender: Any) {
        addNewEntry()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(pickerData[row])"
    }
    
}
